# coding=utf-8
# pip install requests bs4 lxml
import re
import requests
import bs4
import lxml
import codecs
import sys

# 添加其余代码所在位置
sys.path.append("./Tools/")
import SCPSeries
import SCPItem

def main(url):
    # 要爬取得url
    # url = "http://scp-wiki-cn.wikidot.com/"

    # web = SCPSeries.SCPSeries(url)

    web = []
    baseurl = "http://scp-wiki-cn.wikidot.com/scp-"
    for i in range(1, 5000) :
        tempurl = baseurl
        ti = i
        ni = 0
        while ti > 0 :
            ti = int(ti / 10)
            ni += 1
        if(ni < 3):
            ni = 3-ni
            for j in range(0, ni):
                tempurl += "0"
        
        tempurl += str(i)
        web.append(tempurl)

    with codecs.open("res.txt", "w", "utf-8") as OutPutFile:
        i = int(0)
        for e in web:
            item = SCPItem.SCPItem(e)
            OutPutFile.write(item.title + "\n")
            OutPutFile.write(item.Load()+"\n")

            i += 1
            print("has load" + str(i))
        OutPutFile.close()

    # with codecs.open("res.txt", "w", "utf-8") as OutPutFile:
    #     for e in web:
    #         # item = SCPItem.SCPItem(e)
    #         # OutPutFile.write(item.title + "\n")
    #         # OutPutFile.write(item.Load()+"\n")
    #         OutPutFile.write(e+"\n")
    #     OutPutFile.close()

if __name__ == '__main__':
    # url = input("please enter the url of chapters list in www.80txt.com:")
    url = "http://scp-wiki-cn.wikidot.com/scp-series"
    main(url)