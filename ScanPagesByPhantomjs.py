url = "https://www.80txt.com/"

import sys
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
 
def main():
    dcap = dict(DesiredCapabilities.PHANTOMJS)  #设置userAgent
    dcap["phantomjs.page.settings.userAgent"] = ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:25.0) Gecko/20100101 Firefox/25.0 ")
    
    # for i in range(0, len(sys.argv), 1):
    #     print("args %d:" % i, sys.argv[i], sep = " ")
    obj = webdriver.PhantomJS(executable_path='C:\\Users\\lisuzhenxin\\AppData\\Local\\Programs\\Python\\Python36\\Scripts\\phantomjs.exe',desired_capabilities=dcap) #加载网址
    # obj.set_page_load_timeout(30)

    # novelName = input("please enter the name of the novel:")
    novelName = "牧神记"

    searchUrl = "https://cn.bing.com/search?q=site%3Awww.80txt.com+"+novelName

    obj.get(searchUrl)#打开网址

    print(searchUrl)

    searchRess = obj.find_elements_by_class_name("b_algo")

    print(len(searchRess))

    # obj.save_screenshot("1.png")   #截图保存

    # no = obj.find_element_by_id("right")
    # no = no.find_elements_by_class_name("rt_block")
    # no = no[0]
    # no = no.find_element_by_tag_name("h2").find_element_by_tag_name("a")
    # no.click()

    obj.save_screenshot("click.png")

    obj.quit()  # 关闭浏览器。当出现异常时记得在任务浏览器中关闭PhantomJS，因为会有多个PhantomJS在运行状态，影响电脑性能url = "https://www.80txt.com/"

if __name__ == '__main__':
    main()