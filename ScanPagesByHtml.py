# coding=utf-8
# pip install requests bs4 lxml
import re
import requests
import bs4
import lxml
import codecs
import sys
import time

# 添加其余代码所在位置
sys.path.append("./Tools/")
import Chapter
import Novel
import threading

def main(url):
    # 要爬取得url
    # url = "https://www.80txt.com/txtml_71215.html"
    RecordTime = time.time()

    novel = Novel.Novel(url)

    novel.LoadAllChaptersByThreading()

    with codecs.open("res.txt", "w", "utf-8") as OutPutFile:
        OutPutFile.write(novel.title+"-80s\n")
        for li in novel.chapters :
            hastry = 0
            while not li.ready :
                time.sleep(1)
                hastry += 1
                if(hastry > 30):
                    raise RuntimeError("No Response from "+li.url)
            OutPutFile.write(li.title+"\n"+li.content+"\n")
    OutPutFile.close()
    
    RecordTime = time.time() - RecordTime
    print("Cost "+str(RecordTime)+"s.")

if __name__ == '__main__':
    url = input("please enter the url of chapters list in www.80txt.com:\n")
    main(url)