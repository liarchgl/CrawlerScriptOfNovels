import requests
import bs4

class Chapter:
    url = ""
    title = ""
    ready = False
    content = ""

    def LoadFromUrl(self, session=requests.session()):
        if(self.ready == True):
            return self.content
            
        print("loading "+self.title)

        # 获取请求结果
        result = session.get(self.url)

        # 设置编码
        result.encoding = "utf8"

        # 获取html
        html = result.text

        # 使用beautiful soup处理html
        soup = bs4.BeautifulSoup(html)

        # 查找id为content得标签，其内容即为本章内容
        self.content = soup.find(id="content").text.replace("\t"," ")

        self.ready = True

        print("loading "+self.title + " over")

        return self.content

    def __init__(self, title, chapterUrl):
        self.url = chapterUrl
        self.title = "第"+title+"章"