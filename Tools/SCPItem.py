import requests
import bs4

class SCPItem :
    title = ""
    url = ""

    def Load(self):
        session = requests.session()

        result = session.get(self.url)

        html = result.text

        soup = bs4.BeautifulSoup(html)

        self.title = soup.find_all(id="page-title")[0].string

        content = soup.find_all(id="page-content")[0]

        return content.text

    def __init__(self, url):
        self.url = url