import requests
import bs4
import re

class SCPSeries:
    __pattern = re.compile(r"http[s]?://.*/", re.I)
    __baseUrl = ""
    url = ""

    def LoadFromUrl(self):
        # 计算基础url
        self.__baseUrl = self.__pattern.search(self.url)

        session = requests.session()

        result = session.get(self.url)
        html = result.text

        soup = bs4.BeautifulSoup(html)

        content = soup.find_all(id="page-content")[0]

        content = content.find_all(class_="content-panel")[0]

        urls = []

        for e in content.children:
            if e.name == 'ul' :
                for li in e.find_all(name="li"):
                    urls.append(self.__baseUrl.group()+li.a["href"])

        return urls
    
    def __init__(self, url):
        self.url = url