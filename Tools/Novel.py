import bs4
import requests
import concurrent.futures
import Chapter

class Novel:
    url = ""
    chapters = []
    title = ""

    def __LoadInfo(self, session):
        # 通过session可以设置一些请求参数

        # 获取请求结果
        result = session.get(self.url)

        # 获取html
        result.encoding = "utf8"
        html = result.text

        # 使用beautiful soup解析html
        soup = bs4.BeautifulSoup(html)

        # 获取章节列表
        chapterTags = soup.find(id="yulan").find_all(name="li")
        for i in chapterTags :
            chapter = Chapter.Chapter(i.a.string, i.a["href"])
            self.chapters.append(chapter)

        # 获取文章名
        titleTag = soup.find_all(id="titlename")[0]

        self.title = titleTag.h1.string

    def LoadAllChaptersByThreading(self, session=requests.session()):
        with concurrent.futures.ThreadPoolExecutor(40) as exe:
            futures = [exe.submit(chapter.LoadFromUrl, (session)) for chapter in self.chapters]

    def __init__(self, url, session = requests.session()):
        self.url = url
        self.__LoadInfo(session)