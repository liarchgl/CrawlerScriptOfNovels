# Environment

- the editor I use is VSCode
- use `installRequires.bat` or `installRequires.sh` to install python moudules

# Usage

## Just scan statis HTML

- run `ScanPagesByHtml.py`
- enter the url of novel chapters in [80txt](www.80txt.com)
- wait a little seconds when it load from url, which is slow because I didn't use multithread
- `res.txt` is the result

## Scan dynamicly with js running
this script is not complete
- run `ScanPagesByPhantomjs.py`